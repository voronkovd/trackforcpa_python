# -*- coding: utf-8 -*-
import json
import redis
import urlparse
from hashlib import md5
from django.conf import settings
from django.http import HttpResponseRedirect
from click import tasks
from click.models import Rules


def index(request):
    return HttpResponseRedirect(redirect_to=settings.WEB_URL)


def rules(request, id, token):
    token = str(token)
    hashes = md5()
    hashes.update(settings.SECRET_KEY + str(id))
    wrap_id = str(hashes.hexdigest())
    if wrap_id != token:
        return HttpResponseRedirect(redirect_to=settings.WEB_URL)
    try:
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        cache_data = str(r.get(token))
        if cache_data:
            try:
                cache_data = json.loads(cache_data)
                redirect_url = cache_data['rule']['link']['url']
            except Exception:
                redirect_url = settings.WEB_URL
        else:
            rule = Rules.objects.get(id=id, token=token, status=1)
            redirect_url = str(rule.link.url)
        data = {'url': get_refer_url(request), 'ip': get_client_ip(request), 'os': request.user_agent.os.family,
                'browser': request.user_agent.browser.family}
        if request.user_agent.is_mobile:
            data['device_type'] = 2
        elif request.user_agent.is_tablet:
            data['device_type'] = 3
        else:
            data['device_type'] = 1
        if request.user_agent.is_bot:
            data['is_bot'] = 1
        tasks.add.delay(data, id, token)
        return HttpResponseRedirect(redirect_to=redirect_url)
    except Rules.DoesNotExist:
        return HttpResponseRedirect(redirect_to=settings.WEB_URL)


def get_client_ip(request):
    remote_address = request.META.get('REMOTE_ADDR')
    ip = remote_address
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        proxies = x_forwarded_for.split(',')
        if len(proxies) > 0:
            ip = proxies[0]
    return ip


def get_refer_url(request):
    if 'HTTP_REFERER' in request.META:
        return urlparse.urlparse(request.META['HTTP_REFERER']).hostname
    else:
        return ''


