from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'click.settings')

app = Celery(
    'click',
    broker='redis://',
    backend='redis://127.0.0.1:6379/0',
    include=['click.tasks']
)
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
app.conf.update(CELERY_TASK_RESULT_EXPIRES=3600, CELERY_RESULT_BACKEND = 'redis://127.0.0.1:6379/0')