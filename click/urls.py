# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import patterns, url
from django.views.generic import TemplateView

urlpatterns = \
    patterns('',
             url(r'^(?P<id>\d+)/(?P<token>\w+)$', 'click.views.rules'),
             url(r'^$', 'click.views.index'),
             (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
             (r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),

    )