# -*- coding: utf-8 -*-
import os
from variables import db, web, cache, log, debug

ADMINS = (('Admin Track For Cpa', '3407001@gmail.com'))

ALLOWED_HOSTS = ['*']

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

CACHES = cache.CACHE

DATABASES = db.DB

DEBUG = debug.DEBUG

INSTALLED_APPS = ('click', 'django.contrib.staticfiles', 'django_geoip', 'django_user_agents', 'djcelery')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'django.utils.log.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'production': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': log.LOG_FILE_PATH,
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 5,
            'formatter': 'verbose',
        },

    },
    'loggers': {
        'django': {
            'handlers': ['null'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.request': {
            'handlers': ['production'],
            'level': 'ERROR',
            'propagate': False,
        },
        'click': {
            'handlers': ['console', 'production'],
            'level': 'ERROR',
            'filters': ['require_debug_false']
        }
    }
}

MIDDLEWARE_CLASSES = ('django.middleware.common.CommonMiddleware',
                      'django.middleware.clickjacking.XFrameOptionsMiddleware',
                      'django_user_agents.middleware.UserAgentMiddleware')

ROOT_URLCONF = 'click.urls'

SECRET_KEY = 'ue*v6&=rpppl(5wep+5^)b@bqxpt@4_5s$w(6dpoa$vq9c!xp+'

SITE_ROOT = os.path.dirname(os.path.realpath(__file__))

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

STATICFILES_DIRS = (SITE_ROOT + '/static/')

STATIC_ROOT = 'static/'

STATIC_URL = '/static/'

TEMPLATE_DIRS = (os.path.join(SITE_ROOT, 'templates/'))

TEMPLATE_DEBUG = debug.DEBUG

TEMPLATE_LOADERS = ('django.template.loaders.filesystem.Loader',
                    'django.template.loaders.app_directories.Loader')

TIME_ZONE = 'Europe/Moscow'

USE_TZ = True

WSGI_APPLICATION = 'click.wsgi.application'

WEB_URL = web.URL