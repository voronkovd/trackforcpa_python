from djcelery import celery
from click.models import Rules, Refers, Clicks, Browsers, Oses
from django_geoip.models import IpRange


@celery.task
def add(data, id, token):
    rule = Rules.objects.get(id=id, token=token, status=1)
    rule_id = int(id)
    user_id = int(rule.link.user.pk)
    link_id = int(rule.link.pk)
    Refers.objects.get_or_create(url=data['url'], user_id=user_id)
    refer = Refers.objects.get(url=data['url'], user_id=user_id)
    refer_id = int(refer.pk)
    args = dict(rule_id=rule_id, link_id=link_id, refer_id=refer_id, user_id=user_id)
    args['ip'] = data['ip']
    Browsers.objects.get_or_create(name=data['browser'])
    browser = Browsers.objects.get(name=data['browser'])
    args['browser_id'] = int(browser.pk)
    Oses.objects.get_or_create(name=data['os'])
    os = Oses.objects.get(name=data['os'])
    args['os_id'] = int(os.pk)
    args['device_type'] = data['device_type']
    if 'is_bot' in data:
        args['is_bot'] = 1
    try:
        geoip_record = IpRange.objects.by_ip(data['ip'])
        args['geo_ip_ip_range_id'] = geoip_record.id
    except IpRange.DoesNotExist:
        args['geo_ip_ip_range_id'] = 0
    Clicks.objects.create(**args)
