# -*- coding: utf-8 -*-
from django.db import models


class Browsers(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=32)

    class Meta:
        managed = False
        db_table = 'browsers'


class Categories(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey('Users')
    name = models.CharField(max_length=32)

    class Meta:
        managed = False
        db_table = 'categories'


class Clicks(models.Model):
    rule = models.ForeignKey('Rules')
    link = models.ForeignKey('Links')
    refer = models.ForeignKey('Refers')
    user = models.ForeignKey('Users')
    geo_ip_ip_range_id = models.IntegerField()
    ip = models.CharField(max_length=15, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    order = models.IntegerField(blank=True, null=True)
    browser = models.ForeignKey('Browsers')
    os = models.ForeignKey('Oses')
    device_type = models.IntegerField(blank=True)
    is_bot = models.CharField(max_length=32, blank=True)

    class Meta:
        managed = False
        db_table = 'clicks'


class Events(models.Model):
    id = models.IntegerField(primary_key=True)
    rule = models.ForeignKey('Links')
    link = models.ForeignKey('Rules')
    type = models.IntegerField()
    value = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'events'


class Imports(models.Model):
    id = models.IntegerField(primary_key=True)
    link = models.ForeignKey('Links')
    user = models.ForeignKey('Users')
    ip = models.CharField(max_length=15, blank=True)
    created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'imports'


class Links(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey('Users')
    name = models.CharField(max_length=32)
    url = models.CharField(max_length=253)

    class Meta:
        managed = False
        db_table = 'links'


class Oses(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=32)

    class Meta:
        managed = False
        db_table = 'oses'


class Refers(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey('Users')
    url = models.CharField(max_length=253)

    class Meta:
        managed = False
        db_table = 'refers'


class Rules(models.Model):
    id = models.IntegerField(primary_key=True)
    category = models.ForeignKey(Categories)
    link = models.ForeignKey(Links)
    name = models.CharField(max_length=32)
    token = models.CharField(max_length=32)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rules'


class Users(models.Model):
    id = models.IntegerField(primary_key=True)
    email = models.CharField(max_length=32)
    password = models.CharField(max_length=32)
    last_login = models.DateTimeField()
    status = models.IntegerField(blank=True, null=True)
    activation = models.CharField(max_length=32)
    date_registration = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'users'